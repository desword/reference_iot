# reference_iot


## concurrent_decoding

1. Mayank Jain; Jung Il Choi; Taemin Kim; Dinesh Bharadia; Siddharth Seth; Kannan Srinivasan; Philip Levis; Sachin Katti; Prasun Sinha; "Practical, real-time, full duplex wireless." in Proc. of the ACM MobiCom 2011.
2. Bharadia, Dinesh, Emily McMilin, and Sachin Katti. "Full duplex radios." in Proc. of the ACM SIGCOMM 2013 conference on SIGCOMM. 2013.
3. Ahmed, Elsayed, and Ahmed M. Eltawil. "All-digital self-interference cancellation technique for full-duplex systems." IEEE Transactions on Wireless Communications 14.7 (2015): 3519-3532.
4. Wei Wang ; Tiantian Xie ; Xin Liu ; Ting Zhu. "ECT: Exploiting Cross-Technology Concurrent Transmission for Reducing Packet Delivery Delay in IoT Networks" in Proc. of the IEEE INFOCOM 2018.
5. Sujay  Narayana; R  Muralishankar; R Venkatesha Prasad; Vijay S Rao. "Recovering Bits from Thin Air Demodulation of Bandpass Sampled Noisy Signals for Space IoT" in Proc. ACM IPSN 2019.
6.  Zhe Wang, Linghe Kong and Kangjie Xu; Liang He; Kaishun Wu; Guihai Chen. "Online Concurrent Transmissions at LoRa Gateway" in Proc. IEEE INFOCOM 2020.

1. Souvik Sen; Naveen Santhapuri ; Romit Roy Choudhury ; Srihari Nelakuditi. "Successive interference cancellation: Carving out MAC layer opportunities." in IEEE Transactions on Mobile Computing 12.2 (2012): 346-357.
2. Kong, Linghe; Xue Liu. "mZig: Enabling multi-packet reception in ZigBee." in Proc. of mobiCom. 2015.
3. Long Cheng ; Yu Gu ; Jianwei Niu ; Ting Zhu ; Cong Liu ; Qingquan Zhang ; Tian Hel. "Taming collisions for delay reduction in low-duty-cycle wireless sensor networks." in Proc. of IEEE INFOCOM 2016.
4.  Marcelis, Paul J., Vijay S. Rao, and R. Venkatesha Prasad. "DaRe: Data recovery through application layer coding for LoRaWAN." in Proc. of IEEE IoTDI 2017.
5. Shuai Wang; Song Min Kim; Linghe Kong; Tian He. "Concurrent transmission aware routing in wireless networks." IEEE Transactions on Communications 66.12 (2018): 6275-6286.
6. Xiong Wang ; Linghe Kong ; Liang He ; Guihai Chen. "mLoRa: A Multi-Packet Reception Protocol in LoRa networks" in Proc. of ICNP 2019.

## interf_sense_rateless_coding

    [1]Ruirong Chen and Wei Gao. 2019. Enabling Cross-Technology Coexistence for Extremely Weak Wireless Devices. In Proc. of IEEE INFOCOM.
    [2]Steven Siying Hong and Sachin Rajsekhar Katti. 2011. DOF: A Local Wireless Information Plane. In Proc. of SIGCOMM.
    [3]Chieh Jan Mike Liang, Nissanka Bodhi Priyantha, Jie Liu, and Andreas Terzis. 2010. Surviving Wi-Fi Interference in Low Power ZigBee Networks. In Proc. of ACM SenSys.
    [4]Hossein Shafagh James Gross Simon Duquennoy Anwar Hithnawi, Su Li. 2016. CrossZig: Combating Cross-Technology Interference in Low-power Wireless Networks. In Proc. of ACM/IEEE IPSN.
    [5]F. et al Hermans. 2013. SoNIC: Classifying interference in 802.15.4 sensor networks. In Proc. of ACM/IEEE IPSN.
    [6]Jin Meng and et al. 2016. Smogy-Link:Fingerprinting Interference for Predictable Wireless Concurrency. In Proc. of IEEE ICNP.

## network_structure
    [1] Joseph Polastre, Jonathan Hui, Philip Levis, Jerry Zhao, David Culler, Scott Shenker, and Ion Stoica, “ A unifying link abstraction for wireless sensor networks,” In Proc. of ACM SenSys, 2005.
    [2] Cheng Tien Ee, Rodrigo Fonseca, Sukun Kim, Daekyeong Moon, Arsalan Tavakoli, David Culler, Scott Shenker, and Ion Stoica, “A modular network layer for sensorsets,” In Proc. of USENIX OSDI, 2006.
    [3] Kevin Klues, Gregory Hackmann, Octav Chipara, and Chenyang Lu, “A component-based architecture for power-efficient media access control in wireless sensor networks,” In Proc. of ACM SenSys, 2007. 
    [4] Taeseop Lee, Myung-Sup Lee, Hyung-Sin Kim, and Saewoong Bahk, “A synergistic architecture for rpl over ble,” In Proc. of IEEE SECON, 2016.
    [5] Michael Spork, Carlo Alberto Boano, Marco Zimmerling, and Kay Romer, “Bleach: Exploiting the full potential of ipv6 over ble in constrained embedded iot devices,” In Proc. of ACM SenSys, 2017.
    [6] Steffen Thielemans, Maite Bezunartea, and Kris Steenhaut, “Establishing transparent ipv6 communication on lora based low power wide area networks (lpwans),” In Proc. of IEEE WTS, 2017.
    [7] Hudson Ayers, Paul Thomas Crews, Hubert Hua Kian Teo, Conor McAvity, Amit Levy, and Philip Levis, “Design considerations for low power internet protocols,” In Proc. of ACM SenSys, 2018.


## protocol_coexistence
    [1] Kim, Song Min, and Tian He. "Freebee: Cross-technology communication via free side-channel." Proceedings of the 21st Annual International Conference on Mobile Computing and Networking. 2015.
    - FreeBeeCross-technology Communication.pdf
    [2] Guo, Xiuzhen, Xiaolong Zheng, and Yuan He. "Wizig: Cross-technology energy communication over a noisy channel." IEEE INFOCOM 2017-IEEE Conference on Computer Communications. IEEE, 2017.
    - WiZig CrossTechnology Energy Communication.pdf
    [3] Won, Chulho, et al. "Adaptive radio channel allocation for supporting coexistence of 802.15. 4 and 802.11 b." IEEE Vehicular Technology Conference. Vol. 62. No. 4. IEEE; 1999, 2005.
    - Adaptive_radio_channel_allocation_for_su.pdf
    [4] Duquennoy, Simon, et al. "Orchestra: Robust mesh networks through autonomously scheduled TSCH." Proceedings of the 13th ACM conference on embedded networked sensor systems. 2015.
    - Orchestra Robust Mesh Networks.pdf
    [5] Kellogg, Bryce, et al. "Wi-Fi backscatter: Internet connectivity for RF-powered devices." Proceedings of the 2014 ACM conference on SIGCOMM. 2014.
    - WiFi Backscatter.pdf
    [6] Schulz, Matthias, et al. "Shadow Wi-Fi: Teaching smartphones to transmit raw signals and to extract channel state information to implement practical covert channels over Wi-Fi." Proceedings of the 16th Annual International Conference on Mobile Systems, Applications, and Services. 2018.
    - 2018 mobisys shadow wifi teaching smartphones to transmit raw signals and to extract CSI.pdf